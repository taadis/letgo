# Strategy Pattern

策略模式是一种行为模式.

## 应用场景

- 商超活动策略
- 计算器策略
- ...

## 参考

- [策略模式的概念以及代码推演](https://www.bilibili.com/video/BV17i4y147Ve)
- [策略模式实战模拟](https://www.bilibili.com/video/BV1bQ4y1A7P3)
- [每周一个GoLang设计模式之策略模式](https://www.golangtc.com/t/57744181b09ecc02f70001a3)
- [go语言实现设计模式（一）：策略模式](https://studygolang.com/articles/5199)
- [设计模式-策略模式(Go语言描述)](https://blog.csdn.net/qibin0506/article/details/50565061)